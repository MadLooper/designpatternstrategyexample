/**
 * Created by KW on 8/16/17.
 */
public class Main {
    public static void main(String[] args) {
        Hero h = new Hero("zenek", 100);
//        h.setStrategiaWalki(new SwordStrategy());
        h.setStrategiaWalki(new ArrowsStrategy());
        h.fightUsingStrategy();
    }
}
